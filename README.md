# azrs-tracking

Evidencija o alatima koji su korišćeni pri razvoju projekta na kursu "Razvoj softvera".

Projekat nad kojim su primenjivani alati se nalazi na sledećem linku: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/17-tankattack 

Link ka kanban tabli gde je prikazana primena alata: https://gitlab.com/nikolamicic/azrs-tracking/-/boards


# Prikazani alati

1. Sistem za verzionisanje softvera koji smo koristili na projektu je Git - GitFlow (pogledati na https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/17-tankattack)
2. CMake
3. Git hooks
4. GDB
5. Clang Tidy
6. GammaRay
7. Clang Format
8. Qt Creator
9. HotSpot
10. Docker
11. GCov
